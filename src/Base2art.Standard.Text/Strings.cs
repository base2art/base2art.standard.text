﻿namespace Base2art.Text
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class Strings
    {
        private const StringComparison DefaultComparison = StringComparison.Ordinal;

        public static string AsString(this IEnumerable<char> chars) => new string((chars ?? new char[0]).ToArray());

        public static string RemoveSuffix(this string value, string suffix) => RemoveSuffix(value, suffix, DefaultComparison);

        public static string RemoveSuffix(this string value, string suffix, StringComparison comp)
        {
            while (value.EndsWith(suffix, comp))
            {
                value = value.Remove(value.Length - suffix.Length);
            }

            return value;
        }

        public static string RemovePrefix(this string value, string prefix) => RemovePrefix(value, prefix, DefaultComparison);

        public static string RemovePrefix(this string value, string prefix, StringComparison comp)
        {
            while (value.StartsWith(prefix, comp))
            {
                value = value.Substring(prefix.Length);
            }

            return value;
        }

        public static string Join(this string[] values) => string.Join("", values);

        public static string Join(this IEnumerable<string> values) => string.Join("", values);

        public static string Join(this string[] values, string joiner) => string.Join(joiner, values);

        public static string Join(this IEnumerable<string> values, string joiner) => string.Join(joiner, values);

        public static bool StartsWithAny(this string value, IEnumerable<string> values)
            => value.StartsWithAnyInternal(values, DefaultComparison);

        public static bool StartsWithAny(this string value, StringComparison comp, IEnumerable<string> values)
            => value.StartsWithAnyInternal(values, comp);

        public static bool StartsWithAny(this string value, params string[] values)
            => value.StartsWithAnyInternal(values, DefaultComparison);

        public static bool StartsWithAny(this string value, StringComparison comp, params string[] values)
            => value.StartsWithAnyInternal(values, comp);

        private static bool StartsWithAnyInternal(this string value, IEnumerable<string> values, StringComparison comp)
            => values.Any(x => value.StartsWith(x, comp));

        public static bool IsWildCardMatch(this string wildCard, string input, bool ignoreCase) =>
            WildCardToRegular(wildCard, ignoreCase).IsMatch(input);

        private static Regex WildCardToRegular(string value, bool ignoreCase)
        {
            var matcher = "^" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + "$";
            if (ignoreCase)
            {
                return new Regex(matcher, RegexOptions.IgnoreCase);
            }

            return new Regex(matcher);
        }
    }
}

//        public static bool In<T>(this T item, params T[] values)
//            where T : IComparable<T>
//            => (values ?? new T[0]).Any(x => item.CompareTo(x) == 0);
//
//        public static bool In<T>(this T item, IComparer<T> comp, params T[] values)
//            => (values ?? new T[0]).Any(x => comp.Compare(item, x) == 0);