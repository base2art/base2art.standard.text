namespace Base2art.Standard.Text.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Text;
    using FluentAssertions;
    using Xunit;

    public class StringFeatures
    {
        [Fact]
        public void ShouldJoinArray()
        {
            var items = new[] {"1", "2", "3"};

            items.Join().Should().Be("123");
            items.Join(",").Should().Be("1,2,3");
        }

        [Fact]
        public void ShouldJoinEnumerable()
        {
            IEnumerable<string> items = new[] {"1", "2", "3"};

            items.Join().Should().Be("123");
            items.Join(",").Should().Be("1,2,3");
        }

        [Fact]
        public void ShouldSuffix()
        {
            "Abc-rc1".RemoveSuffix("-rc1").Should().Be("Abc");
            "Abc-rc1-rc1".RemoveSuffix("-rc1").Should().Be("Abc");

            "Abc-Rc1".RemoveSuffix("-rc1").Should().Be("Abc-Rc1");
            "Abc-RC1-Rc1".RemoveSuffix("-rc1").Should().Be("Abc-RC1-Rc1");

            "Abc-Rc1".RemoveSuffix("-rc1", StringComparison.Ordinal).Should().Be("Abc-Rc1");
            "Abc-RC1-Rc1".RemoveSuffix("-rc1", StringComparison.Ordinal).Should().Be("Abc-RC1-Rc1");
            "Abc-rc1".RemoveSuffix("-rc1", StringComparison.Ordinal).Should().Be("Abc");

            "Abc-Rc1".RemoveSuffix("-rc1", StringComparison.OrdinalIgnoreCase).Should().Be("Abc");
            "Abc-RC1-Rc1".RemoveSuffix("-rc1", StringComparison.OrdinalIgnoreCase).Should().Be("Abc");
        }

        [Fact]
        public void ShouldPrefix()
        {
            "rc1-Abc".RemovePrefix("rc1-").Should().Be("Abc");
            "rc1-Abc".RemovePrefix("rc1-").Should().Be("Abc");

            "Rc1-Abc".RemovePrefix("rc1-").Should().Be("Rc1-Abc");
            "RC1-Rc1-Abc".RemovePrefix("rc1-").Should().Be("RC1-Rc1-Abc");

            "Rc1-Abc".RemovePrefix("rc1-", StringComparison.Ordinal).Should().Be("Rc1-Abc");
            "RC1-Rc1-Abc".RemovePrefix("rc1-", StringComparison.Ordinal).Should().Be("RC1-Rc1-Abc");
            "rc1-Abc".RemovePrefix("rc1-", StringComparison.Ordinal).Should().Be("Abc");

            "Rc1-Abc".RemovePrefix("rc1-", StringComparison.OrdinalIgnoreCase).Should().Be("Abc");
            "RC1-Rc1-Abc".RemovePrefix("rc1-", StringComparison.OrdinalIgnoreCase).Should().Be("Abc");
        }

        [Fact]
        public void ShouldStartWithAny()
        {
            "rc1-Abc".StartsWithAny("rc2-", "rc1-").Should().BeTrue();
            "rc1-Abc".StartsWithAny("rc2-", "Rc1-").Should().BeFalse();
            "rc1-Abc".StartsWithAny(StringComparison.OrdinalIgnoreCase, "rc2-", "Rc1-").Should().BeTrue();

            "rc1-Abc".StartsWithAny(new List<string>() {"rc2-", "rc1-"}).Should().BeTrue();
            "rc1-Abc".StartsWithAny(new List<string>() {"rc2-", "Rc1-"}).Should().BeFalse();
            "rc1-Abc".StartsWithAny(StringComparison.OrdinalIgnoreCase, new List<string>() {"rc2-", "Rc1-"}).Should().BeTrue();
        }

        [Theory]
        [InlineData("*-abc", "rc1-Abc", true, true)]
        [InlineData("???-abc", "rc1-Abc", true, true)]
        [InlineData("????-abc", "rc1-Abc", true, false)]
        [InlineData("*-abc", "rc1-abc", false, true)]
        [InlineData("*-abc", "rc1-Abc", false, false)]
        [InlineData("???-abc", "rc1-Abc", false, false)]
        [InlineData("????-abc", "rc1-Abc", false, false)]
        public void ShouldIsWildcardMatch(string wildcard, string input, bool ignoreCase, bool expected)
        {
            wildcard.IsWildCardMatch(input, ignoreCase).Should().Be(expected);
        }

        [Fact]
        public void ShouldAsString()
        {
            "Scott".Where((x, i) => i % 2 == 0)
                   .AsString()
                   .Should()
                   .Be("Sot");
        }
    }
}